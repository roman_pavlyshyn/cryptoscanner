<?php

namespace App\Notification;

use Illuminate\Database\Eloquent\Model;

class ThirtyMin extends Model {

    protected $table = 'notification-thirty-min';

    /**
     * primaryKey 
     * 
     * @var integer
     * @access protected
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'market',
        'symbol',
        'from_price',
        'to_price',
        'volume',
        'percent',
    ];

}
