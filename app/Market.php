<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Market extends Model {

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'domain',
        'logo',
        'updated_at',
    ];

    public function baseCurrencies() {
        return $this->belongsToMany(\App\BaseCurrency::class)
                    ->withTimestamps();
    }
    
    public function users() {
        return $this->belongsToMany(\App\User::class)
                    ->withTimestamps();
    }
}
