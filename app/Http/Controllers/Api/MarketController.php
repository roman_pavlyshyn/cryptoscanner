<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use App\Http\Controllers\Controller;
use App\Market;
use App\User;

class MarketController extends Controller {

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $thisSecond = Market::with('baseCurrencies')->get();
        
        echo json_encode($thisSecond->toArray());
    }
}