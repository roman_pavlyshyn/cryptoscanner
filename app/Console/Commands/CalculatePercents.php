<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Market;

class CalculatePercents extends Command {
    
    use \App\Console\Commands\ManageCache;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'calculate:percents {market}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Calculate percent changeged "calculate:percents {market}"';
    
    
    protected $marketModel = null;
    
    
    protected $symbolsModel = null;
    
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $market = $this->argument('market');
        $sleepSeconds = 1;
        
        switch($market) {
            case 'hitbtc':
                $this->marketModel = \App\Market\HitBTC::class;
                $this->symbolsModel = \App\Market\HitBTCSymbols::class;
                $sleepSeconds = 0.1;
                break;
            case 'binance':
                $this->marketModel = \App\Market\Binance::class;
                $this->symbolsModel = \App\Market\BinanceSymbols::class;
                $sleepSeconds = 0.1;
                break;
            case 'bitfinex':
                $this->marketModel = \App\Market\Bitfinex::class;
                $this->symbolsModel = \App\Market\BitfinexSymbols::class;
                break;
            case 'bittrex':
                $this->marketModel = \App\Market\Bittrex::class;
                $this->symbolsModel = \App\Market\BittrexSymbols::class;
                $sleepSeconds = 1;
                break;
            case 'cryptopia':
                $this->marketModel = \App\Market\Cryptopia::class;
                $this->symbolsModel = \App\Market\CryptopiaSymbols::class;
                $sleepSeconds = 4;
                break;
            case 'liqui':
                $this->marketModel = \App\Market\Liqui::class;
                $this->symbolsModel = \App\Market\LiquiSymbols::class;
                $sleepSeconds = 1;
                break;
        }
        
        
        
        
        
        while(true) {
            $marketData = $this->getMarketData($market);
            
            if(is_array($marketData) && sizeof($marketData)) {
                $dataToInsert = [];
                foreach($marketData as $symbol => $data) {
                    $oldPrices = $this->getOldPrices($symbol);

                    $dataToInsert = [
                        'symbol' => $symbol,
                        'price' => $data['price'],
                        'volume' => number_format(($data['volume']*$data['price']), 3, '.', ''),
                        'p5m' => $this->percentChanged($data['price'], $oldPrices['price5Mago']),
                        'p30m' => $this->percentChanged($data['price'], $oldPrices['price30Mago']),
                        'p1h' => $this->percentChanged($data['price'], $oldPrices['price1Hago']),
                        'p4h' => $this->percentChanged($data['price'], $oldPrices['price4Hago']),
                        'p24h' => $this->percentChanged($data['price'], $oldPrices['price24Hago']),
                    ];
                    $this->symbolsModel::updateOrCreate(['symbol' =>$symbol], $dataToInsert)->save();
                }
                
                Market::where('name', $market)->update(['updated_at'=> \Carbon\Carbon::now()]);
                
                usleep($sleepSeconds*1000000);
            }
        }
    }

    private function getOldPrices($symbol) {
        $time5 = \Carbon\Carbon::now()->subMinutes(5)->format('Y-m-d H:i');
        $time30 = \Carbon\Carbon::now()->subMinutes(30)->format('Y-m-d H:i');
        $time1h = \Carbon\Carbon::now()->subHour()->format('Y-m-d H:i');
        $time4h = \Carbon\Carbon::now()->subHours(4)->format('Y-m-d H:i');
        $time24h = \Carbon\Carbon::now()->subHours(24)->format('Y-m-d H:i');

        $records = $this->marketModel::select(['price', 'created_at'])
            ->where('symbol', '=', $symbol)
            ->where(function ($query) {
                
                $query->where(function ($query) {
                    $time5 = \Carbon\Carbon::now()->subMinutes(5)->format('Y-m-d H:i:s');
                    $query->where('created_at', '<=', $time5)
                        ->orderBy('created_at', 'asc')
                        ->limit(1);
                })
                ->orWhere(function ($query) {
                    $time30 = \Carbon\Carbon::now()->subMinutes(30)->format('Y-m-d H:i:s');
                    $query->where('created_at', '<=', $time30)
                        ->orderBy('created_at', 'asc')
                        ->limit(1);
                })
                ->orWhere(function ($query) {
                    $time1h = \Carbon\Carbon::now()->subHour()->format('Y-m-d H:i:s');
                    $query->where('created_at', '>=', $time1h)
                        ->orderBy('created_at', 'asc')
                        ->limit(1);
                })
                ->orWhere(function ($query) {
                    $time4h = \Carbon\Carbon::now()->subHours(4)->format('Y-m-d H:i:s');
                    $query->where('created_at', '>=', $time4h)
                        ->orderBy('created_at', 'asc')
                        ->limit(1);
                })
                ->orWhere(function ($query) {
                    $time24h = \Carbon\Carbon::now()->subHours(24)->format('Y-m-d H:i:s');
                    $query->where('created_at', '>=', $time24h)
                        ->orderBy('created_at', 'asc')
                        ->limit(1);
                });
            })
            ->get();

        $result = [
            'price5Mago' => 0,
            'price30Mago' => 0,
            'price1Hago' => 0,
            'price4Hago' => 0,
            'price24Hago' => 0,
        ];
        if (isset($records[0])) {
            foreach ($records as $record) {
                if ($record->created_at <= $time5) {
                    $result['price5Mago'] = floatval($record->price);
                } elseif ($record->created_at <= $time30) {
                    $result['price30Mago'] = floatval($record->price);
                } elseif ($record->created_at <= $time1h) {
                    $result['price1Hago'] = floatval($record->price);
                } elseif ($record->created_at <= $time4h) {
                    $result['price4Hago'] = floatval($record->price);
                } elseif ($record->created_at <= $time24h) {
                    $result['price24Hago'] = floatval($record->price);
                }
            }
        }
        return $result;
    }

    private function percentChanged($start, $end) {
        return ($end != 0 && $start != 0) ? ((($start - $end) / $end) * 100) : 0;
    }

}