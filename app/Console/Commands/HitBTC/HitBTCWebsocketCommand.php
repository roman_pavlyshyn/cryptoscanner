<?php

namespace App\Console\Commands\HitBTC;

use Illuminate\Console\Command;

class HitBTCWebsocketCommand extends Command {

    use \App\Console\Commands\ManageCache;
    
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tickers:hitbtc';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Connect to the HitBTC websocket and put prices to the cashe';

    /**
     * @var currency pairs
     */
    protected $instruments;
    
    
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $symbols = json_decode(file_get_contents('https://api.hitbtc.com/api/2/public/symbol'), 1);
        foreach ($symbols as $symbol) {
            $this->instruments[] = $symbol['id'];
        }

        $loop = \React\EventLoop\Factory::create();
        $connector = new \Ratchet\Client\Connector($loop);
        $connector('wss://api.hitbtc.com/api/2/ws')
            ->then(function(\Ratchet\Client\WebSocket $conn) {
                foreach ($this->instruments as $ins) {
                    $conn->send('{"method": "subscribeTicker","params": {"symbol": "' . $ins . '"}}');
                }
                $conn->on('message', function(\Ratchet\RFC6455\Messaging\MessageInterface $msg) use ($conn) {

                    $data = json_decode($msg, 1);
                    
                    if (isset($data['params'])) {
                        $this->manageCacheArray(
                            'hitbtc',
                            $data['params']['symbol'], // Symbol
                            floatval($data['params']['last']), // Current day's close price
                            floatval($data['params']['bid']),
                            floatval($data['params']['ask']),
                            floatval($data['params']['volume'])
                        );
                    }
                });
                $conn->on('close', function($code = null, $reason = null) {
                    /** log errors here */
                    echo "Connection closed ({$code} - {$reason})\n";
                });
            }, function(\Exception $e) use ($loop) {
                /** hard error */
                echo "Could not connect: {$e->getMessage()}\n";
                $loop->stop();
            });
        $loop->run();
    }
}
