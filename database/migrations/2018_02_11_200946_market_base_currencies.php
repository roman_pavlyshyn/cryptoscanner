<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MarketBaseCurrencies extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('base-currencies', function (Blueprint $table) {
            $table->increments('id');
            $table->string('currency', 255)->unique();
            $table->timestamps();
        });
        
        Schema::create('base_currency_market', function (Blueprint $table) {
            $table->integer('market_id')->unsigned();
            $table->integer('base_currency_id')->unsigned();
            $table->timestamps();
            
            $table->foreign('market_id')->references('id')
                ->on('markets')->onDelete('cascade');
            
            $table->foreign('base_currency_id')->references('id')
                ->on('base-currencies')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('base_currency_market');
        Schema::dropIfExists('base-currencies');
    }

}
