<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel {

    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\HitBTC\HitBTCWebsocketCommand::class,
        Commands\Binance\BinanceWebsocketCommand::class,
        Commands\BitfinexWebsocketCommand::class,
        Commands\LiquiCommand::class,
        Commands\CryptopiaCommand::class,
        Commands\BittrexCommand::class,
        Commands\PoloniexWebsocketCommand::class,
        
        Commands\SavePerSecond::class,
        Commands\CalculatePercents::class,
        Commands\CalculatePercents5min::class,
        Commands\CalculatePercents15min::class,
        Commands\CheckUpdates::class,
        
        Commands\ClearOldCommand::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule) {
        $markets = [
            'binance',
            'bitfinex',
            'bittrex',
            'cryptopia',
            'hitbtc',
            'liqui',
            //'poloniex'
        ];
        
        if (!$this->osProcessIsRunning('laravel-echo-server start')) {
            $schedule->exec('laravel-echo-server start')->everyMinute();
        }
        

        if (!$this->osProcessIsRunning('queue:listen')) {
            $schedule->command('queue:listen')->everyMinute();
        }
        
        if (!$this->osProcessIsRunning('check:updates')) {
            $schedule->command('check:updates')->everyMinute();
        }
        
        foreach ($markets as $market) {
            if (!$this->osProcessIsRunning('tickers:'.$market)) {
                $schedule->command('tickers:'.$market)->everyMinute();
            }
            if (!$this->osProcessIsRunning('save:persecond '.$market)) {
                $schedule->command('save:persecond '.$market)->everyMinute();
            }
            if (!$this->osProcessIsRunning('calculate:percents5min '.$market)) {
                $schedule->command('calculate:percents5min '.$market)->everyMinute();
            }
            if (!$this->osProcessIsRunning('calculate:percents15min '.$market)) {
                $schedule->command('calculate:percents15min '.$market)->everyMinute();
            }
            if (!$this->osProcessIsRunning('calculate:percents '.$market)) {
                $schedule->command('calculate:percents '.$market)->everyMinute();
            }
        }
        
        $schedule->command('clear:olddata')->everyThirtyMinutes();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands() {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }
    
    /**
     * checks, if a process with $needle in the name is running
     *
     * @param string $needle
     * @return bool
     */
    protected function osProcessIsRunning($needle) {   
        // get process status. the "-ww"-option is important to get the full output!
        exec('ps aux -ww', $process_status);
        
        // search $needle in process status
        $result = array_filter($process_status,
            function($var) use ($needle)
            {   return strpos($var, $needle); });


        // if the result is not empty, the needle exists in running processes
        if (!empty($result)) {
            return true;
        }
        return false;
    }
}
