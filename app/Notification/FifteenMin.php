<?php

namespace App\Notification;

use Illuminate\Database\Eloquent\Model;

class FifteenMin extends Model {

    protected $table = 'notification-fifteen-min';

    /**
     * primaryKey 
     * 
     * @var integer
     * @access protected
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'market',
        'symbol',
        'from_price',
        'to_price',
        'volume',
        'percent',
    ];

}
