import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex);

const settings = {
    state: {
        checked_markets : [],
        checked_currencies: [],
        checked_time_frame : {
            '5M' : {
                percChange: -10,
                minVol: 5
            },
            '15M' : {
                percChange: -10,
                minVol: 5
            },
            '30M' : {
                percChange: -10,
                minVol: 5
            },
            '1H' : {
                percChange: -10,
                minVol: 5
            },
            '4H' : {
                percChange: -10,
                minVol: 5
            },
            '24H' : {
                percChange: -10,
                minVol: 5
            }
        },
        
    },
    mutations: {
        FETCH(state, settings) {
            var m = [];
            for (var i in settings) {
                var key = settings[i]['key'];
                var value = settings[i]['value'];
                state['checked_'+key] = JSON.parse(value);
            }
        },
        UPDATE_MARKETS(state, markets) {
            state.checked_markets = markets;
        },
        UPDATE_TIME_FRAME(state, time_frame) {
            state.checked_time_frame = time_frame;
        },
        UPDATE_CURRENCIES(state, currencies) {
            state.checked_currencies = currencies;
        },
    },
    actions: {
        fetchAll({ commit, state })  {
            return new Promise((resolve, reject) => {
                axios.get("/user/settings").then((response) => {
                    commit("FETCH", response.data.data);
                    resolve(response);
                    
                }).catch((error => {
                    reject(error);
                }));
            });
        },
        updateMarkets({commit}, markets) {
            return new Promise((resolve, reject) => {
                axios.post("/user/settings", {markets: markets})
                .then((response) => {
                    commit('UPDATE_MARKETS', response.data.markets)
                }).catch((error => {
                    reject(error)
                }))
            })
        },
        updateCurrencies({commit}, currencies) {
            return new Promise((resolve, reject) => {
                axios.post("/user/settings", {currencies: currencies})
                .then((response) => {
                    commit('UPDATE_CURRENCIES', response.data.currencies)
                }).catch((error => {
                    reject(error)
                }))
            })
        },
        updateTimeFrame({commit, state}) {
            return new Promise((resolve, reject) => {
                axios.post("/user/settings", {'time_frame': state.checked_time_frame})
                .then((response) => {
                    commit('UPDATE_TIME_FRAME', response.data.time_frame)
                }).catch((error => {
                    reject(error)
                }))
            })
        }
    },
    getters: {
        checkedMarkets(state) {
            return state.checked_markets;
        },
        selectedTimeFrame(state) {
            return state.checked_time_frame;
        },
        checkedCurrencies(state) {
            return state.checked_currencies;
        },
    }
}

export default new Vuex.Store({
    modules: {
        settings: settings
    }
})
