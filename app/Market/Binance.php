<?php

namespace App\Market;

use Illuminate\Database\Eloquent\Model;

class Binance extends Model {

    protected $table = 'binance';

    /**
     * primaryKey 
     * 
     * @var integer
     * @access protected
     */
    protected $primaryKey = null;

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'symbol',
        'price',
        'bid',
        'ask'
    ];

}
