<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'api', 'prefix' => 'auth'], function ($router) {
    Route::post('register', 'Api\AuthController@register');
    Route::post('login', 'Api\AuthController@login');
    Route::get('refresh', 'Api\AuthController@refresh');
    Route::get('user', 'Api\AuthController@user');
});

Route::group(['middleware' => 'api'], function () {
    Route::get('markets', 'Api\MarketController@index');
    
    Route::group(['middleware' => 'jwt.auth'], function()   {
        Route::apiResource('user/settings', 'Api\UserSettingsController');
    });
});
