<?php

namespace App\Market;

use Illuminate\Database\Eloquent\Model;

class HitBTCSymbols extends Model {

    protected $table = 'hitbtc-symbols';

    /**
     * primaryKey 
     * 
     * @var integer
     * @access protected
     */
    protected $primaryKey = 'id';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'symbol',
        'price',
        'volume',
        'p5m',
        'p15m',
        'p30m',
        'p1h',
        'p4h',
        'p24h',
    ];

}
