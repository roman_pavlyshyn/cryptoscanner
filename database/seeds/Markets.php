<?php

use Illuminate\Database\Seeder;

class Markets extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $markets = [
            [
                'data' => [
                    'name' => 'binance',
                    'domain' => 'www.binance.com',
                    'logo' => '/img/binance.png',
                ],
                'currencies' => [
                    'BTC', 'ETH', 'BNB', 'USDT'
                ]
            ],
            [
                'data' => [
                    'name' => 'bitfinex',
                    'domain' => 'www.bitfinex.com',
                    'logo' => '/img/bitfinex.png',
                ],
                'currencies' => [
                    'BTC', 'ETH', 'USD'
                ]
            ],
            [
                'data' => [
                    'name' => 'bittrex',
                    'domain' => 'bittrex.com',
                    'logo' => '/img/bittrex.png',
                ],
                'currencies' => [
                    'BTC', 'ETH', 'USDT'
                ]
            ],
            [
                'data' => [
                    'name' => 'cryptopia',
                    'domain' => 'www.cryptopia.co.nz',
                    'logo' => '/img/cryptopia.png',
                ],
                'currencies' => [
                    'BTC', 'USDT', 'NZDT', 'LTC', 'DOGE'
                ]
            ],
            [
                'data' => [
                    'name' => 'hitbtc',
                    'domain' => 'hitbtc.com',
                    'logo' => '/img/hitbtc.png',
                ],
                'currencies' => [
                    'BTC', 'ETH', 'USDT'
                ]
            ],
            [
                'data' => [
                    'name' => 'liqui',
                    'domain' => 'liqui.io',
                    'logo' => '/img/liqui.png',
                ],
                'currencies' => [
                    'BTC', 'ETH', 'USDT'
                ]
            ],
            [
                'data' => [
                    'name' => 'poloniex',
                    'domain' => 'poloniex.com',
                    'logo' => '/img/poloniex.png',
                ],
                'currencies' => [
                    'BTC', 'ETH', 'XMR', 'USDT'
                ]
            ],
        ];

        foreach ($markets as $market) {
            $cMarket = App\Market::create($market['data']);
            
            foreach ($market['currencies'] as $currency) {
                $res = App\BaseCurrency::where('currency', $currency)->get();
                
                
                if(isset($res[0])) {
                    $currency = $res[0];
                }
                else {
                    $currency = App\BaseCurrency::create([
                        'currency' => $currency
                    ]);
                }
                
                $cMarket->baseCurrencies()->attach($currency->id);
            }
        }
    }

}
