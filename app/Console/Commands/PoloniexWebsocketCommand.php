<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class PoloniexWebsocketCommand extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tickers:poloniex';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Connect to the Poloniex websocket and put prices to the cashe';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $loop = \React\EventLoop\Factory::create();
        $connector = new \Ratchet\Client\Connector($loop);
        $connector('wss://api.poloniex.com:443')
        ->then(function(\Ratchet\Client\WebSocket $conn) {
            $conn->send('{"event": "subscribe","channel":"ticker","pair": "BTC_ETH"}');
            
            
            $conn->on('message', function(\Ratchet\RFC6455\Messaging\MessageInterface $msg) use ($conn) {

                $data = json_decode($msg, 1);
                
                print_r($data);
                
            });
            $conn->on('close', function($code = null, $reason = null) {
                /** log errors here */
                echo "Connection closed ({$code} - {$reason})\n";
            });
        }, function(\Exception $e) use ($loop) {
            /** hard error */
            echo "Could not connect: {$e->getMessage()}\n";
            $loop->stop();
        });
        $loop->run();
    }

}
