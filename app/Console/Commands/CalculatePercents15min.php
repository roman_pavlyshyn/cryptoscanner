<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Market;

class CalculatePercents15min extends Command {
    
    use \App\Console\Commands\ManageCache;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'calculate:percents15min {market}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Calculate percent changeged "calculate:percents15min {market}"';
    
    
    protected $marketModel = null;
    
    
    protected $symbolsModel = null;
    
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $market = $this->argument('market');
        $sleepSeconds = 1;
        
        switch($market) {
            case 'hitbtc':
                $this->marketModel = \App\Market\HitBTC::class;
                $this->symbolsModel = \App\Market\HitBTCSymbols::class;
                $sleepSeconds = 0.1;
                break;
            case 'binance':
                $this->marketModel = \App\Market\Binance::class;
                $this->symbolsModel = \App\Market\BinanceSymbols::class;
                $sleepSeconds = 0.1;
                break;
            case 'bitfinex':
                $this->marketModel = \App\Market\Bitfinex::class;
                $this->symbolsModel = \App\Market\BitfinexSymbols::class;
                break;
            case 'bittrex':
                $this->marketModel = \App\Market\Bittrex::class;
                $this->symbolsModel = \App\Market\BittrexSymbols::class;
                $sleepSeconds = 1;
                break;
            case 'cryptopia':
                $this->marketModel = \App\Market\Cryptopia::class;
                $this->symbolsModel = \App\Market\CryptopiaSymbols::class;
                $sleepSeconds = 4;
                break;
            case 'liqui':
                $this->marketModel = \App\Market\Liqui::class;
                $this->symbolsModel = \App\Market\LiquiSymbols::class;
                $sleepSeconds = 1;
                break;
        }
        
        while(true) {
            $marketData = $this->getMarketData($market);
            
            if(is_array($marketData) && sizeof($marketData)) {
                $dataToInsert = [];
                foreach($marketData as $symbol => $data) {
                    $oldPrices = $this->getOldPrices($symbol);

                    $dataToInsert = [
                        'symbol' => $symbol,
                        'price' => $data['price'],
                        'volume' => number_format(($data['volume']*$data['price']), 3, '.', ''),
                        'p15m' => $this->percentChanged($data['price'], $oldPrices['price15Mago'])
                    ];
                    $this->symbolsModel::updateOrCreate(['symbol' =>$symbol], $dataToInsert)->save();
                }
                
                Market::where('name', $market)->update(['updated_at'=> \Carbon\Carbon::now()]);
                usleep($sleepSeconds*1000000);
            }
        }
    }

    private function getOldPrices($symbol) {
        $time15 = \Carbon\Carbon::now()->subMinutes(15)->format('Y-m-d H:i:s');

        $record = $this->marketModel::select(['price', 'created_at'])
            ->where('symbol', '=', $symbol)
            ->where('created_at', '<=', $time15)
            ->orderBy('created_at', 'desc')
            ->first();
        
        $result['price15Mago'] = (isset($record->price))? floatval($record->price): 0;
        
        return $result;
    }

    private function percentChanged($start, $end) {
        return ($end != 0 && $start != 0) ? ((($start - $end) / $end) * 100) : 0;
    }

}