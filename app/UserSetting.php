<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Tymon\JWTAuth\Facades\JWTAuth;

class UserSetting extends Model {

    protected $table = 'user-settings';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'key',
        'value',
    ];
    
    public function scopeAuth($query) {
        $user = JWTAuth::parseToken()->authenticate();
        return $query->where('user-settings.user_id', '=', $user->id);
    }
}
