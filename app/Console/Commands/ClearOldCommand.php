<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Market\Binance;
use App\Market\Bitfinex;
use App\Market\Bittrex;
use App\Market\Cryptopia;
use App\Market\HitBTC;
use App\Market\Liqui;

class ClearOldCommand extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'clear:olddata';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clear records older 24 hours from Data Base';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $min24h = \Carbon\Carbon::now()->subHours(2)->format('Y-m-d H:i:s');

        Binance::where('created_at', '<', $min24h)->delete();
        Bitfinex::where('created_at', '<', $min24h)->delete();
        Bittrex::where('created_at', '<', $min24h)->delete();
        Cryptopia::where('created_at', '<', $min24h)->delete();
        HitBTC::where('created_at', '<', $min24h)->delete();
        Liqui::where('created_at', '<', $min24h)->delete();
    }

}
