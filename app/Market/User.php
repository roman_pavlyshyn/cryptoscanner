<?php

namespace App\Market;

use Illuminate\Database\Eloquent\Model;

class User extends Model {
    
    protected $table = 'market-user';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'market_id',
        'user_id',
    ];
}
