
import Home from './components/home.vue';
import Login from './components/Auth/Login.vue';

export default {
    mode: 'history',
    routes: [
        {
            path: '/', name: 'home', component: Home,
            meta: { auth: true }
        },
        {
            path: '/login', name: 'login', component: Login,
            meta: { auth: false }
        }
    ]
}