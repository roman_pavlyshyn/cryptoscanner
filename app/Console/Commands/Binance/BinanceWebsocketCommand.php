<?php

namespace App\Console\Commands\Binance;

use Illuminate\Console\Command;

class BinanceWebsocketCommand extends Command {

    use \App\Console\Commands\ManageCache;
    
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tickers:binance';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Connect to the Binance websocket and put prices to the cashe';

    private $symbols = [];
    
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $symbols = json_decode(file_get_contents('https://api.binance.com/api/v1/ticker/allPrices'), 1);

        foreach ($symbols as $symbol) {
            $this->symbols[] = $symbol['symbol'];
        }

        $srteamsLine = 'stream?streams=';
        foreach ($this->symbols as $symbol) {
            $srteamsLine .= strtolower($symbol) . '@ticker/';
        }

        $loop = \React\EventLoop\Factory::create();
        $connector = new \Ratchet\Client\Connector($loop);

        $app = function (\Ratchet\Client\WebSocket $conn) use (&$connector, &$loop, &$app, &$srteamsLine) {
            $conn->on('message', function(\Ratchet\RFC6455\Messaging\MessageInterface $msg) use ($conn) {

                $ticker = json_decode($msg, 1);
                if (isset($ticker['data']['s']) && isset($ticker['data']['c']) && isset($ticker['data']['v'])) {
                    
                    $this->manageCacheArray(
                            'binance', 
                            $ticker['data']['s'], // Symbol
                            floatval($ticker['data']['c']), // Current day's close price
                            floatval($ticker['data']['b']),
                            floatval($ticker['data']['a']),
                            floatval($ticker['data']['v'])
                    );
                }
            });
            $conn->on('close', function($code = null, $reason = null) use (&$connector, &$loop, &$app, &$srteamsLine) {
                echo "Connection closed ({$code} - {$reason})\n";

                $loop->addTimer(1, function () use (&$connector, &$loop, &$app, &$srteamsLine) {
                    connectToServer($connector, $loop, $app, $srteamsLine);
                });
            });
        };

        function connectToServer(&$connector, &$loop, &$app, &$srteamsLine) {
            $connector('wss://stream.binance.com:9443/' . $srteamsLine)
                ->then($app, function (\Exception $e) use ($loop) {
                    echo "Could not connect: {$e->getMessage()}\n";
                    $loop->stop();
                });
        }

        connectToServer($connector, $loop, $app, $srteamsLine);
        $loop->run();
    }
}
