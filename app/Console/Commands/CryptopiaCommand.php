<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class CryptopiaCommand extends Command {

    use ManageCache;
    
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tickers:cryptopia';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get info from Cryptopia API';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        while (true) {
            $result = json_decode(file_get_contents('https://www.cryptopia.co.nz/api/GetMarkets'), 1);

            foreach ($result['Data'] as $k => $market) {
                if ($market['Label']) {
                    $this->manageCacheArray(
                        'cryptopia',
                        $market['Label'],
                        floatval($market['LastPrice']),
                        floatval($market['BidPrice']),
                        floatval($market['AskPrice']),
                        floatval($market['Volume'])
                    );
                }
            }

            sleep(5);
        }
    }

}
