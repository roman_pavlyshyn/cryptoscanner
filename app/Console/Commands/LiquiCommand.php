<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class LiquiCommand extends Command {

    use ManageCache;
    
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tickers:liqui';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get info from Liqui API';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $markets = json_decode(file_get_contents('https://api.liqui.io/api/3/info'), 1);

        $queriString = 'https://api.liqui.io/api/3/ticker/';
        $tickers = [];
        foreach ($markets['pairs'] as $t => $market) {
            $tickers[] = $t;
        }

        while (true) {
            $result = json_decode(file_get_contents($queriString.implode('-', $tickers)), 1);
            
            foreach($result as $symbol=>$data) {
                if(isset($data['last']) && $data['vol'])
                $this->manageCacheArray(
                        'liqui',
                        $symbol,
                        floatval($data['last']),
                        floatval($data['sell']),
                        floatval($data['buy']),
                        floatval($data['vol'])
                    );
            }
            
            sleep(2);
        }
    }
}
