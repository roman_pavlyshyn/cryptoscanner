<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BaseCurrency extends Model {

    protected $table = 'base-currencies';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'currency'
    ];

    public function markets() {
        return $this->belongsToMany(\App\Market::class)
                    ->withTimestamps();
    }

}
