<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\UserSetting;

class UserSettingsController extends Controller {
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        $settings = UserSetting::auth()->get();
        
        return response([
            'status' => 'success',
            'data' => $settings->jsonSerialize()
        ], Response::HTTP_OK);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $data = $request->all();
        $userId = Auth::user()->id;
        $result = [];

        
        foreach($data as $key=> $setting) {
            $dataToInsert = [
                'user_id' => $userId,
                'key' => $key,
                'value' => json_encode($setting)
            ];

            $settings = UserSetting::updateOrCreate([
                'user_id' => $userId,
                'key' =>  $key
            ], $dataToInsert)->save();

            $result = [
                'status' => 'success',
                $key => $setting
            ];
        }
        
        return response($result, Response::HTTP_OK);
    }
}
