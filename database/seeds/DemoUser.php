<?php

use Illuminate\Database\Seeder;

class DemoUser extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        App\User::create([
            'name' => 'Demo User',
            'email' => 'demo@demo.com',
            'password' => bcrypt('demo@demo.com'),
        ]);
    }

}
