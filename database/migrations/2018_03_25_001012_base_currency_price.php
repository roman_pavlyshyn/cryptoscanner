<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BaseCurrencyPrice extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        
        Schema::table('base_currency_market', function (Blueprint $table) {
            $table->string('btc_price', 32)->after('base_currency_id')->default(1);
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('base_currency_market', function (Blueprint $table) {
            $table->dropColumn('btc_price');
        });
    }

}
