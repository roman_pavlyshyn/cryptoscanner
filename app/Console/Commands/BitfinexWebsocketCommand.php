<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class BitfinexWebsocketCommand extends Command {
    
    use ManageCache;
    
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tickers:bitfinex';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Connect to the Bitfinex websocket and put prices to the cashe';
    
    /**
     * @var currency pairs
     */
    protected $instruments;
    
    /**
     * @var array
     */
    public $channels = array();
    
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * @param $ch
     * @param $id
     * @param $ticker
     *
     *  Channels are not always the same chan number
     */
    public function updChannels($ch, $id, $tk) {
        $this->channels[$id] = [
            'channel' => $ch,
            'ticker' => $tk
        ];
    }
    
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $symbols = json_decode(file_get_contents('https://api.bitfinex.com/v1/symbols_details'), 1);

        foreach($symbols as $symbol) {
            $this->instruments[] = 't'.strtoupper($symbol['pair']);
        }
        
        $loop = \React\EventLoop\Factory::create();
        $connector = new \Ratchet\Client\Connector($loop);
        $connector('wss://api.bitfinex.com/ws/2')
                ->then(function(\Ratchet\Client\WebSocket $conn) {
                    foreach ($this->instruments as $ins) {
                        $conn->send('{"event": "subscribe","channel":"ticker","symbol": "' . $ins . '"}');
                    }
                    $conn->on('message', function(\Ratchet\RFC6455\Messaging\MessageInterface $msg) use ($conn) {

                        $data = json_decode($msg, 1);
                        if ((!empty($data['event']) && $data['event'] == 'subscribed') ? true : false) {
                            if (!empty($data['channel'])) {
                                $this->updChannels($data['channel'], $data['chanId'], $data['symbol']);
                            }
                        }

                        if (!empty($data[0]) && $data[1] <> 'hb') {
                            $data[0] = $this->channels[$data[0]]; // set data[0] to book/ticker/trade
                            
                            
                            /** -------------- TICKER -------------- */
                            /** Ticker
                             *  BID,
                              BID_SIZE,
                              ASK,
                              ASK_SIZE,
                              DAILY_CHANGE,
                              DAILY_CHANGE_PERC,
                              LAST_PRICE,
                              VOLUME,
                              HIGH,
                              LOW
                             * 
                             * */
                            if ($data[0]['channel'] == 'ticker') {
                                $this->manageCacheArray(
                                    'bitfinex',
                                    $data[0]['ticker'],
                                    $data[1][6],
                                    $data[1][0],
                                    $data[1][2],
                                    $data[1][7]
                                );
                            }
                        }
                    });
                    $conn->on('close', function($code = null, $reason = null) {
                        /** log errors here */
                        echo "Connection closed ({$code} - {$reason})\n";
                    });
                }, function(\Exception $e) use ($loop) {
                    /** hard error */
                    echo "Could not connect: {$e->getMessage()}\n";
                    $loop->stop();
                });
        $loop->run();
    }
}
