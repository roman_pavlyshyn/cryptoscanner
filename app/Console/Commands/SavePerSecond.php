<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Market;

class SavePerSecond extends Command {

    use \App\Console\Commands\ManageCache;
    
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'save:persecond {market}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Save data from markets in to DB "save:persecond {market}"';

    
    protected $marketModel = null;
    
    protected $baseCurrencies = [];
    
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $market = $this->argument('market');
        $sleepSeconds = 1;
        
        switch($market) {
            case 'hitbtc':
                $this->marketModel = \App\Market\HitBTC::class;
                break;
            case 'binance':
                $this->marketModel = \App\Market\Binance::class;
                break;
            case 'bitfinex':
                $this->marketModel = \App\Market\Bitfinex::class;
                break;
            case 'bittrex':
                $this->marketModel = \App\Market\Bittrex::class;
                $sleepSeconds = 2;
                break;
            case 'cryptopia':
                $this->marketModel = \App\Market\Cryptopia::class;
                $sleepSeconds = 5;
                break;
            case 'liqui':
                $this->marketModel = \App\Market\Liqui::class;
                $sleepSeconds = 2;
                break;
        }
        
        
        $baseMarket = Market::where('name', $market)->with('baseCurrencies')->first();
        $this->baseCurrencies = $baseMarket->baseCurrencies;
        
        
        while(true) {
            $marketData = $this->getMarketData($market);
            
            if(is_array($marketData) && sizeof($marketData)) {
                $dataToInsert = [];
                foreach($marketData as $symbol => $data) {
                    $dataToInsert[] = [
                        'symbol' => $symbol,
                        'price' => $data['price'],
                        'bid' => $data['bid'],
                        'ask' => $data['ask']
                    ];
                    $this->updateBaseCurrency($symbol, $data['price']);
                }
                
                $this->marketModel::insert($dataToInsert);
            }
            
            sleep($sleepSeconds);
        }
    }
    
    /*
     * 
     */
    private function updateBaseCurrency($symbol, $price) {
        
        foreach($this->baseCurrencies as &$baseCurrency) {
            if($baseCurrency->currency != 'BTC') {
                if(
                        //binance, hitbtc
                        ($symbol == $baseCurrency->currency.'BTC' || $symbol == 'BTC'.$baseCurrency->currency)
                            ||
                        //bitfinex
                        ($symbol == 'tBTC'.$baseCurrency->currency || $symbol == 't'.$baseCurrency->currency.'BTC')
                            ||
                        //bittrex
                        ($symbol == $baseCurrency->currency.'-BTC' || $symbol == 'BTC-'.$baseCurrency->currency)
                            ||
                        //cryptopia
                        ($symbol == $baseCurrency->currency.'/BTC' || $symbol == 'BTC/'.$baseCurrency->currency)
                            ||
                        //hitbtc (USD)
                        ($symbol == 'BTCUSD' && $baseCurrency->currency == 'USDT')
                            ||
                        //liqui
                        ($symbol == strtolower($baseCurrency->currency).'_btc' || $symbol == 'btc_'.strtolower($baseCurrency->currency))
                            ||
                        //poloniex
                        ($symbol == $baseCurrency->currency.'_BTC' || $symbol == 'BTC_'.$baseCurrency->currency)
                ){
                    
                    $baseCurrency->pivot->btc_price = $price;
                    $baseCurrency->pivot->save();
                }
            }
        }
    }

}
