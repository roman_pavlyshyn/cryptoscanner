<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class BittrexCommand extends Command {

    use ManageCache;
    
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tickers:bittrex';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get info from Bittrex API';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        
        while (true) {
            $markets = json_decode(file_get_contents('https://bittrex.com/api/v1.1/public/getmarketsummaries'), 1);
            foreach ($markets['result'] as $market) {
                if ($market['MarketName']) {
                    $this->manageCacheArray(
                        'bittrex',
                        $market['MarketName'],
                        floatval($market['Last']),
                        floatval($market['Bid']),
                        floatval($market['Ask']),
                        floatval($market['Volume'])
                    );
                }
            }

            sleep(2);
        }
    }

}
