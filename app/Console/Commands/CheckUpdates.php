<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Events\MarketUpdated;
use App\Market;
use App\BaseCurrency;
use App\UserSetting;

class CheckUpdates extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check:updates';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check updates in DB and call events';
    
    protected $symbolsModel = null;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        while (true) {
            $now = \Carbon\Carbon::now()->subSeconds(7)->format('Y-m-d H:i:s');
            $markets = Market::where('updated_at', '>=', $now)->get()->toArray();

            if (sizeof($markets) > 0) {
                event(new MarketUpdated($markets));
            }

            foreach ($markets as $market) {
                $this->checkPerUser($market['name']);
            }

            usleep(600000);
        }
    }

    private function checkPerUser($market) {
        $marketsPerUser = UserSetting::where('key', 'markets')->get()->toArray();

        foreach ($marketsPerUser as $userMarkets) {
            $markets = json_decode($userMarkets['value'], true);


            if (in_array($market, $markets)) {


                $userCurrencies = UserSetting::where('user_id', $userMarkets['user_id'])
                                ->where('key', 'currencies')
                                ->first()->toArray();
                $userTimeFrame = UserSetting::where('user_id', $userMarkets['user_id'])
                                ->where('key', 'time_frame')
                                ->first()->toArray();

                $timeFrames = json_decode($userTimeFrame['value'], true);
                $uCurrencies = json_decode($userCurrencies['value'], true);
                $cs = BaseCurrency::select('currency')->whereIn('id', $uCurrencies)->get()->toArray();
                $currencies = [];
                foreach($cs as $c) {
                   $currencies[] = $c['currency'];
                }

                switch ($market) {
                    case 'hitbtc':
                        $this->symbolsModel = \App\Market\HitBTCSymbols::class;
                        break;
                    case 'binance':
                        $this->symbolsModel = \App\Market\BinanceSymbols::class;
                        break;
                    case 'bitfinex':
                        $this->symbolsModel = \App\Market\BitfinexSymbols::class;
                        break;
                    case 'bittrex':
                        $this->symbolsModel = \App\Market\BittrexSymbols::class;
                        break;
                    case 'cryptopia':
                        $this->symbolsModel = \App\Market\CryptopiaSymbols::class;
                        break;
                    case 'liqui':
                        $this->symbolsModel = \App\Market\LiquiSymbols::class;
                        break;
                }

                
                
                $query = $this->symbolsModel::orderBy('updated_at', 'desc');
                
                foreach ($currencies as $currency) {
                    $query->where('symbol', 'like', '%'.$currency);
                }
                
                $query->where(function ($query) use ($timeFrames) {
                    
                    $i=0;
                    foreach ($timeFrames as $timeFrame=>$value) {
                        $tf = 'p'.strtolower($timeFrame);

                        //print_r($value);

                        if($i=0) {
                            if($value > 0) {
                                $query->where($tf, '<=', $value['percChange']);
                            }
                            elseif($value != 0) {
                                $query->where($tf, '>=', $value['percChange']);
                            }
                        }
                        else {
                            if($value > 0) {
                                $query->orWhere($tf, '<=', $value['percChange']);
                            }
                            elseif($value != 0) {
                                $query->orWhere($tf, '>=', $value['percChange']);
                            }
                        }
                        
                        $i++;
                    }
                });
                
                $filteredCoins = $query->get();
                
                
                print_r($filteredCoins->toArray());
                
                echo "\n";
                
                $userMarkets['user_id'];
                
            }
        }
    }

}
