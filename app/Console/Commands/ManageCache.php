<?php

namespace App\Console\Commands;

trait ManageCache {
    
    private function manageCacheArray($market, $symbol, $price, $bid, $ask, $volume) {
        $currentTime = time();
        $key = $market.'::array';
        $item = [
            'price' => $price,
            'volume' => $volume,
            'bid' => $bid,
            'ask' => $ask,
            'time' => time(),
        ];
        
        if(\Cache::has($key)) {
            $value = \Cache::get($key);
            $value_arr = unserialize(base64_decode($value));
            
            if(isset($value_arr[$symbol]) && $value_arr[$symbol]['time'] !== $item['time']) {
                $value_arr[$symbol] = $item;
                \Cache::put($key, base64_encode(serialize($value_arr)), 1);
            }
            elseif(!isset($value_arr[$symbol])) {
                $value_arr[$symbol] = $item;
                \Cache::put($key, base64_encode(serialize($value_arr)), 1);
            }
        } else {
            $value = array($symbol => $item);
            $value = base64_encode(serialize($value));
            \Cache::add($key, $value, 1);
        }
    }
    
    private function getMarketData($market) {
        $key = $market.'::array';
        $value = \Cache::get($key);
        $data = unserialize(base64_decode($value));
        return $data;
    }
    
}