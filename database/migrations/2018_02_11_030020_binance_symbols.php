<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BinanceSymbols extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('binance-symbols', function (Blueprint $table) {
            $table->increments('id');
            $table->string('symbol', 32);
            $table->string('price', 32);
            $table->string('volume', 32)->nullable()->default(0);
            $table->float('p5m', 8, 3)->nullable()->default(0);
            $table->float('p15m', 8, 3)->nullable()->default(0);
            $table->float('p30m', 8, 3)->nullable()->default(0);
            $table->float('p1h', 8, 3)->nullable()->default(0);
            $table->float('p4h', 8, 3)->nullable()->default(0);
            $table->float('p24h', 8, 3)->nullable()->default(0);
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));

            $table->engine = 'MyISAM';

            $table->index('symbol');
            $table->index('volume');
            $table->index('p5m');
            $table->index('p15m');
            $table->index('p30m');
            $table->index('p1h');
            $table->index('p4h');
            $table->index('p24h');
            $table->index('created_at');
            $table->index('updated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('binance-symbols');
    }

}
