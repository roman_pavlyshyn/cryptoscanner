<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class HitBTCMarket extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('hitbtc', function (Blueprint $table) {
            $table->string('symbol', 32);
            $table->string('price', 32);
            $table->string('bid', 32);
            $table->string('ask', 32);
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            
            $table->engine = 'MyISAM';
            
            $table->index('symbol');
            $table->index('created_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('hitbtc');
    }

}
