<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NotificationThirtyMin extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('notification-thirty-min', function (Blueprint $table) {
            $table->increments('id');
            $table->string('market', 100);
            $table->string('symbol', 32);
            $table->string('from_price', 32);
            $table->string('to_price', 32);
            $table->string('volume', 32)->nullable()->default(0);
            $table->float('percent', 8, 3)->nullable()->default(0);
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));

            $table->index('market');
            $table->index('symbol');
            $table->index('volume');
            $table->index('percent');
        });

        Schema::create('notification-thirty-min-user', function (Blueprint $table) {
            $table->integer('user_id')->unsigned();
            $table->integer('notification_id')->unsigned();
            $table->timestamps();

            $table->foreign('user_id')->references('id')
                    ->on('users')->onDelete('cascade');

            $table->foreign('notification_id')->references('id')
                    ->on('notification-thirty-min')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('notification-thirty-min-user');
        Schema::dropIfExists('notification-thirty-min');
    }

}
