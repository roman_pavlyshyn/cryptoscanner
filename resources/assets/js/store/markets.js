import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex);

const markets = {
    state: {
        markets : [],
        currencies: []
    },
    mutations: {
        FETCH(state, markets) {
            var isset = false;
            for (var key in markets) {
                for (var i in state.markets) {
                    if(markets[key].id === state.markets[i].id) {
                        Vue.set(state.markets, state.markets.indexOf(state.markets[i]), markets[key])
                        isset = true;
                        break;
                    }
                }
                if(!isset) {
                    state.markets.push(markets[key]);
                }
            }
        },
        FETCH_CURRENCIES(state, currencies) {
            var tmp = [];
            for (var key in currencies) {
                tmp.push({
                    id: currencies[key].id,
                    symbol: currencies[key].currency
                });
            }
            for (var key in tmp) {
                var isset = false;
                for (var i in state.currencies) {
                    if(tmp[key].id === state.currencies[i].id) {
                        Vue.set(state.currencies, state.currencies.indexOf(state.currencies[i]), tmp[key])
                        isset = true;
                        break;
                    }
                }
                if(!isset) {
                    state.currencies.push(tmp[key]);
                }
            }
        }
    },
    actions: {
        fetchUpdated({ commit, state })  {
            Echo.channel('market').listen('.update', function (e) {
                commit("FETCH", e.markets)
            });
        },
        
        fetchAll({ commit, state })  {
            var _this = this;

            return new Promise((resolve, reject) => {
                axios.get("/markets").then((response) => {
                    commit("FETCH", response.data);
                    resolve(response.data);
                    
                    var markets = response.data;
                    for (var key in markets) {
                        commit("FETCH_CURRENCIES", markets[key].base_currencies);
                    }
                    
                    _this.dispatch("fetchUpdated");
                }).catch((error => {
                    reject(error);
                }));
            });
        }
    },
    getters: {
        markets(state) {
            return state.markets;
        },
        currencies(state) {
            return state.currencies;
        }
    }
}

export default new Vuex.Store({
    modules: {
        markets: markets
    }
})
